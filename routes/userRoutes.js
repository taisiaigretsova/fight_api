const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function(req, res, next) {
    try {
        const data = UserService.getAll();
        res.data = data;
        res.json(data);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});
router.get('/:id', function(req, res, next) {
    try {
        const data = UserService.search({id: req.params.id});
        res.data = data;
        res.json(data);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.post('/', function(req, res, next) {
    try {
        const data = UserService.create(req.body);
        res.data = data;
        res.json(data);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.put('/:id', function(req, res, next) {
    try {
        const data = UserService.update(req.params.id, req.body);
        res.data = data;
        res.json(data);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

router.delete('/:id', function(req, res, next) {
    try {
        const data = UserService.delete(req.params.id);
        res.data = data;
        res.json(data);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
});

module.exports = router;