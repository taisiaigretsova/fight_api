const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAll(search) {
        const items = UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw new Error('User not found');
        }
        return item;
    }

    create(newUserData) {
        const item = UserRepository.create(newUserData);
        if(!item) {
            throw new Error('Can not create user');
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = UserRepository.update(id, dataToUpdate);
        if(!item) {
            throw new Error('Can not update user');
        }
        return item;
    }

    update(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            throw new Error('Can not delete user');
        }
        return item;
    }
}

module.exports = new UserService();